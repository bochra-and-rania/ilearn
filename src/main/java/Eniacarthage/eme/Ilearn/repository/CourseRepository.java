package Eniacarthage.eme.Ilearn.repository;

import Eniacarthage.eme.Ilearn.model.Course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course,Integer> {
}
