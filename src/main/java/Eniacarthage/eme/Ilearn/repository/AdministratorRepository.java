package Eniacarthage.eme.Ilearn.repository;

import Eniacarthage.eme.Ilearn.model.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {
    @Query("select ad from Administrator ad where ad.eMail = :eMail and ad.password = :password")
    Administrator findByeMailAndpassword(String eMail, String password);

}
