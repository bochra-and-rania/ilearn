package Eniacarthage.eme.Ilearn.repository;

import Eniacarthage.eme.Ilearn.model.Comments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentsRepository extends JpaRepository<Comments,Long> {
}
