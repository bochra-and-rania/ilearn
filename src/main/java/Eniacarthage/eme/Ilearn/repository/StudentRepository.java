package Eniacarthage.eme.Ilearn.repository;

import Eniacarthage.eme.Ilearn.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query("select st from Student st where st.eMail = :eMail and st.password = :password")
    Student findByeMailAndpassword(String eMail, String password);
}
