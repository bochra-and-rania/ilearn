package Eniacarthage.eme.Ilearn.repository;

import Eniacarthage.eme.Ilearn.model.Test;
import Eniacarthage.eme.Ilearn.model.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends JpaRepository<Test,Integer> {
}
