package Eniacarthage.eme.Ilearn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Eniacarthage.eme.Ilearn.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {



}
