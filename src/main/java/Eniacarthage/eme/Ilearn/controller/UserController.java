package Eniacarthage.eme.Ilearn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Eniacarthage.eme.Ilearn.model.User;

@RestController
@RequestMapping("/api/v1/")
public class UserController {
	@Autowired
	private Eniacarthage.eme.Ilearn.repository.UserRepository UserRepository;
	// get all users 
	@GetMapping("/users")
	public List<User> getallusers(){
		return UserRepository.findAll();
	}
}
