package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.ITeacherService;
import Eniacarthage.eme.Ilearn.model.Teacher;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin()
@RestController
@RequestMapping("teacher")
public class TeacherController {

    private final ITeacherService teacherService;

    public TeacherController(ITeacherService teacherService) {
        this.teacherService = teacherService;

    }

    @PostMapping()
    public void addTeacher(@RequestBody Teacher t) {
        teacherService.addTeacher(t);
    }

    @PutMapping()
    public void updateTeacher(@RequestBody Teacher t) {
        teacherService.updateTeacher(t);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteTeacher(@PathVariable("id") int id) {
        teacherService.deleteTeacher(id);
    }

    @GetMapping()
    public List<Teacher> getAllTeacher() {
        return teacherService.getAllTeacher();
    }
}
