package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.ICourseService;
import Eniacarthage.eme.Ilearn.Service.TrainingService;
import Eniacarthage.eme.Ilearn.model.Course;
import Eniacarthage.eme.Ilearn.model.Training;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("course")
public class CourseController {
    private final TrainingService trainingService;

    private final ICourseService iCourseService;

    public CourseController(TrainingService trainingService, ICourseService iCourseService) {
        this.trainingService = trainingService;
        this.iCourseService = iCourseService;

    }

    @PostMapping(value = "/{id}")
    public void addCourse(@RequestBody Course t, @PathVariable int id) {
        Training training = trainingService.getTraining(id);
        t.setTraining(training);

        iCourseService.addCourse(t);
    }

    @PutMapping()
    public void updateCourse(@RequestBody Course t) {
        iCourseService.updateCourse(t);
    }

    @DeleteMapping()
    public void deleteCourse(@PathVariable("id") int id) {
        iCourseService.deleteCourse(id);
    }

    @GetMapping(value = "/get")
    public List<Course> getAllCourse() {
        return iCourseService.getAllCourse();
    }
}
