package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.ILoginService;
import Eniacarthage.eme.Ilearn.dto.LoginDto;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("api/login")
public class LoginController {
    private final ILoginService iLoginService;

    public LoginController(ILoginService iLoginService) {
        this.iLoginService = iLoginService;
    }

    @PostMapping()
    public String login(@RequestBody LoginDto loginDto) {
        return iLoginService.login(loginDto.getEmail(), loginDto.getPassword());
    }

}
