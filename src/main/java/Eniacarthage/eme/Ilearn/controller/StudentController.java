package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.IStudentService;
import Eniacarthage.eme.Ilearn.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("student")
public class StudentController {
    private final IStudentService iStudentService;

    public StudentController(IStudentService iStudentService) {
        this.iStudentService = iStudentService;

    }

    @PostMapping(value = "/addStudent")
    public void addStudent(@RequestBody Student t) {
        iStudentService.addStudent(t);
    }

    @PutMapping(value = "/updateStudent")
    public void updateStudent(@RequestBody Student t) {
        iStudentService.updateStudent(t);
    }

    @DeleteMapping(value = "/deleteStudent/{id}")
    public void deleteStudent(@PathVariable("id") int id) {
        iStudentService.deleteStudent(id);
    }

    @GetMapping(value = "/getStudent")
    public List<Student> getAllStudent() {
        return iStudentService.getAllStudent();
    }
}
