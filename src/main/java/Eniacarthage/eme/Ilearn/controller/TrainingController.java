package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.ITeacherService;
import Eniacarthage.eme.Ilearn.Service.ITrainingService;
import Eniacarthage.eme.Ilearn.model.Teacher;
import Eniacarthage.eme.Ilearn.model.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("training")
public class TrainingController {
    private final ITrainingService iTrainingService;
    @Autowired
    private ITeacherService teacherService;

    public TrainingController(ITrainingService iTrainingService) {
        this.iTrainingService = iTrainingService;

    }

    @PostMapping(value = "/{id}")
    public void addTraining(@RequestBody Training t, @PathVariable int id) {
        Teacher teacher = teacherService.getTeacher(id);
        t.setTeacher(teacher);

        iTrainingService.addTraining(t);
    }

    @PutMapping(value = "/updateTraining")
    public void updateTraining(@RequestBody Training t) {
        iTrainingService.updateTraining(t);
    }

    @DeleteMapping(value = "/deleteTraining/{id}")
    public void deleteTraining(@PathVariable("id") int id) {
        iTrainingService.deleteTraining(id);
    }

    @GetMapping(value = "/getTraining")
    public List<Training> getAllTraining() {
        return iTrainingService.getAllTraining();
    }
}
