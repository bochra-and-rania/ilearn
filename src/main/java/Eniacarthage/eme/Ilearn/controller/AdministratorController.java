package Eniacarthage.eme.Ilearn.controller;

import Eniacarthage.eme.Ilearn.Service.IAdministratorService;
import Eniacarthage.eme.Ilearn.model.Administrator;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("administrator")
public class AdministratorController {

    private final IAdministratorService iAdministratorService;

    public AdministratorController(IAdministratorService iAdministratorService) {
        this.iAdministratorService = iAdministratorService;

    }

    @PostMapping()
    public void addAdminstrator(@RequestBody Administrator t) {
        iAdministratorService.addAdminstrator(t);
    }

    @PutMapping()
    public void updateAdminstrator(@RequestBody Administrator t) {
        iAdministratorService.updateAdminstrator(t);
    }

    @DeleteMapping()
    public void deleteAdminstrator(@PathVariable("id") int id) {
        iAdministratorService.deleteAdminstrator(id);
    }

    @GetMapping()
    public List<Administrator> getAllAdminstrator() {
        return iAdministratorService.getAllAdminstrator();
    }
}
