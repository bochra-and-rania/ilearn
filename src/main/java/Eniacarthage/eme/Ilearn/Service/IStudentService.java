package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Student;

import java.util.List;

public interface IStudentService {

    void addStudent(Student t);  //ajout insert

    void deleteStudent(int id); //delete

    void updateStudent(Student t);

    List<Student> getAllStudent(); //select*
}
