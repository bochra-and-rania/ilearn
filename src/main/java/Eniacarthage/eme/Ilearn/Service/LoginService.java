package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.repository.AdministratorRepository;
import Eniacarthage.eme.Ilearn.repository.StudentRepository;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements ILoginService {

    private final AdministratorRepository administratorRepository;
    private final StudentRepository studentRepository;


    public LoginService(AdministratorRepository administratorRepository, StudentRepository studentRepository) {
        this.administratorRepository = administratorRepository;
        this.studentRepository = studentRepository;

    }

    @Override
    public String login(String email, String password) {
        String result = "error";
        if ((administratorRepository.findByeMailAndpassword(email, password)) != null)
            result = "admin";
        else {
            if ((studentRepository.findByeMailAndpassword(email, password) != null))
                result = "student";
        }

        return result;
    }
}