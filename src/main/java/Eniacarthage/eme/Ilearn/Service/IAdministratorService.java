package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Administrator;

import java.util.List;

public interface IAdministratorService {
    void addAdminstrator(Administrator t);  //ajout insert

    void deleteAdminstrator(int id); //delete

    void updateAdminstrator(Administrator t);

    List<Administrator> getAllAdminstrator();
}
