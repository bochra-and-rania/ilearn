package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Course;

import java.util.List;

public interface ICourseService {

    void addCourse(Course t);  //ajout insert

    void deleteCourse(int id); //delete

    void updateCourse(Course t);

    List<Course> getAllCourse();

}
