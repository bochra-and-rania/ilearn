package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Course;
import Eniacarthage.eme.Ilearn.repository.CourseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService implements ICourseService {

    CourseRepository courseRepository;

    @Override
    public void addCourse(Course t) {
        courseRepository.save(t);
    }

    @Override
    public void deleteCourse(int id) {
        courseRepository.deleteById(id);
    }

    @Override
    public void updateCourse(Course t) {
        courseRepository.save(t);
    }

    @Override
    public List<Course> getAllCourse() {
        return courseRepository.findAll();
    }
}
