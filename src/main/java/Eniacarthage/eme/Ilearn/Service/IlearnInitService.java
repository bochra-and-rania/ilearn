package Eniacarthage.eme.Ilearn.Service;

public interface IlearnInitService {
    public void initTraining();
    public void initTeacher();
    public void initStudent();
    public void initAdministrator();
    public void initCourse();
    public void initInscriprtion();
    public void initComments();
    public void initTest();

}
