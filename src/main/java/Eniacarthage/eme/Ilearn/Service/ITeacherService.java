package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Teacher;

import java.util.List;

public interface ITeacherService {
    void addTeacher(Teacher t);  //ajout insert

    void deleteTeacher(int id); //delete

    void updateTeacher(Teacher t);

    List<Teacher> getAllTeacher(); //select*

    Teacher getTeacher(int id);
}
