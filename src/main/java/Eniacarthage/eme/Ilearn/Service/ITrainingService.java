package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Training;

import java.util.List;

public interface ITrainingService {
    void addTraining(Training t);  //ajout insert

    void deleteTraining(int id); //delete

    void updateTraining(Training t);

    List<Training> getAllTraining();

    Training getTraining(int id);
}
