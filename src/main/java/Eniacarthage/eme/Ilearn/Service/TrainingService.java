package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Training;
import Eniacarthage.eme.Ilearn.repository.TraniningRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingService implements ITrainingService {


    @Autowired
    TraniningRepository traniningRepository;

    @Override
    public void addTraining(Training t) {
        traniningRepository.save(t);
    }

    @Override
    public void deleteTraining(int id) {
        traniningRepository.deleteById(id);
    }

    @Override
    public void updateTraining(Training t) {
        traniningRepository.save(t);
    }

    @Override
    public List<Training> getAllTraining() {
        return traniningRepository.findAll();
    }

    @Override
    public Training getTraining(int id) {
        return traniningRepository.findById(id).get();
    }
}
