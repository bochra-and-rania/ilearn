package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Administrator;
import Eniacarthage.eme.Ilearn.repository.AdministratorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdministratorService implements IAdministratorService {
    AdministratorRepository administratorRepository;

    @Override
    public void addAdminstrator(Administrator t) {
        administratorRepository.save(t);
    }

    @Override
    public void deleteAdminstrator(int id) {
        administratorRepository.deleteById(id);
    }

    @Override
    public void updateAdminstrator(Administrator t) {
        administratorRepository.save(t);
    }

    @Override
    public List<Administrator> getAllAdminstrator() {
        return administratorRepository.findAll();
    }
}
