package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class IlearnInitServiceImpl implements IlearnInitService {
   @Autowired private TeacherRepository TeacherRepository ;
    @Autowired private StudentRepository StudentRepository ;
    @Autowired private AdministratorRepository AdministratorRepository ;
    @Autowired private TraniningRepository TraniningRepository ;
    @Autowired private CourseRepository CourseRepository ;
    @Autowired private CommentsRepository CommentsRepository ;
    @Autowired private TestRepository TestRepository ;
    @Autowired private InscriptionRepository InscriptionRepository ;
    @Autowired private UserRepository UserRepository ;


    @Override
    public void initTraining() {

    }

    @Override
    public void initTeacher() {

    }

    @Override
    public void initStudent() {

    }

    @Override
    public void initAdministrator() {

    }

    @Override
    public void initCourse() {

    }

    @Override
    public void initInscriprtion() {

    }

    @Override
    public void initComments() {

    }

    @Override
    public void initTest() {

    }
}
