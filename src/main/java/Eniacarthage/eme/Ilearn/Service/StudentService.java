package Eniacarthage.eme.Ilearn.Service;


import Eniacarthage.eme.Ilearn.model.Student;
import Eniacarthage.eme.Ilearn.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService implements IStudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public void addStudent(Student t) {
        studentRepository.save(t);
    }

    @Override
    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }

    @Override
    public void updateStudent(Student t) {
        studentRepository.save(t);
    }

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();

    }
}
