package Eniacarthage.eme.Ilearn.Service;

import Eniacarthage.eme.Ilearn.model.Teacher;
import Eniacarthage.eme.Ilearn.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class TeacherService implements ITeacherService {
    @Autowired
    TeacherRepository teacherRepository;

    @Override
    public void addTeacher(Teacher t) { //insert
        teacherRepository.save(t);
    }

    @Override
    public void deleteTeacher(int id) {
        teacherRepository.deleteById(id);
    }

    @Override
    public void updateTeacher(Teacher t) {
        teacherRepository.save(t);
    }

    @Override
    public List<Teacher> getAllTeacher() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher getTeacher(int id) {
        return teacherRepository.findById(id).get();
    }
}
