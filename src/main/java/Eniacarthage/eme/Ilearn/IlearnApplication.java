package Eniacarthage.eme.Ilearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IlearnApplication {
	public static void main(String[] args) {
		SpringApplication.run(IlearnApplication.class, args);
	}
}
