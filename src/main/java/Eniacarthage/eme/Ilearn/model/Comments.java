package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "comments")
public class Comments {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "question")
    private String question;
    @Column(name = "response")
    private String response;


    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Teacher> teachers = new ArrayList<>();


    @ManyToMany()
    private Collection<Student> students1 = new ArrayList<>();

    public Comments(int id, String question, String response, Collection<Teacher> teachers, Collection<Student> students1) {
        this.id = id;
        this.question = question;
        this.response = response;
        this.teachers = teachers;
        this.students1 = students1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Collection<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Collection<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Collection<Student> getStudents1() {
        return students1;
    }

    public void setStudents1(Collection<Student> students1) {
        this.students1 = students1;
    }
}

