package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "test")
public class Test {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "note")
    private int note;


    @ManyToMany()
    private Collection<Student> students = new ArrayList<>();


    @ManyToMany()
    private Collection<Course> courses = new ArrayList<>();


    public Test(int id, int note, Collection<Student> students, Collection<Course> courses) {
        this.id = id;
        this.note = note;
        this.students = students;
        this.courses = courses;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public Collection<Student> getStudents() {
        return students;
    }

    public void setStudents(Collection<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", note=" + note +
                ", students=" + students +
                ", courses=" + courses +
                '}';
    }
}
