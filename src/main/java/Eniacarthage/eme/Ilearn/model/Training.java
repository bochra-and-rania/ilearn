package Eniacarthage.eme.Ilearn.model;


import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "training")
public class Training {
    @ManyToOne
    public Teacher teacher;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "nameTraining")
    private String nameTraining;
    @Column(name = "evaluation")
    private String evaluation;
    @OneToMany(mappedBy = "training")
    private Collection<Course> courses;

    public Training(int id, String nameTraining, String evaluation, Teacher teacher, Collection<Course> courses) {
        this.id = id;
        this.nameTraining = nameTraining;
        this.evaluation = evaluation;
        this.teacher = teacher;
        this.courses = courses;
    }

    public Training() {
    }


    public Collection<Course> getCourses() {
        return courses;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameTraining() {
        return nameTraining;
    }

    public void setNameTraining(String nameTraining) {
        this.nameTraining = nameTraining;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Teacher getTeacher() {
        return teacher;
    }


    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", nameTraining='" + nameTraining + '\'' +
                ", evaluation='" + evaluation + '\'' +
                ", teacher=" + teacher +
                ", courses=" + courses +
                '}';
    }
}
