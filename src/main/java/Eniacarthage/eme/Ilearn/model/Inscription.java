package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "inscription")
public class Inscription {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @ManyToOne
    private Student student;
    @ManyToOne
    private Course course;

    public Inscription(int id, Date date, Student student, Course course) {
        this.id = id;
        this.date = date;
        this.student = student;
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Inscription{" +
                "id=" + id +
                ", date=" + date +
                ", student=" + student +
                ", course=" + course +
                '}';
    }
}
