package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "course")
public class Course {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "number")
    private int number;
    @Column(name = "description")
    private String description;
    @Column(name = "duration")
    private String duration;
    @Column(name = "dateCreation")
    private String dateCreation;


    @ManyToOne
    private Training training;


    @ManyToMany(mappedBy = "courses")
    private Collection<Test> tests = new ArrayList<>();


    @OneToMany(mappedBy = "course")
    private Collection<Inscription> inscriptions;


    public Course(int id, String name, int number, String description, String duration, String dateCreation, Training training, Collection<Test> tests, Collection<Inscription> inscriptions) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.description = description;
        this.duration = duration;
        this.dateCreation = dateCreation;
        this.training = training;
        this.tests = tests;
        this.inscriptions = inscriptions;
    }

    public Course() {
    }

    public Collection<Inscription> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(Collection<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    public Collection<Test> getTests() {
        return tests;
    }

    public void setTests(Collection<Test> tests) {
        this.tests = tests;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number=" + number +
                ", description='" + description + '\'' +
                ", duration='" + duration + '\'' +
                ", dateCreation='" + dateCreation + '\'' +
                ", training=" + training +
                ", tests=" + tests +
                ", inscriptions=" + inscriptions +
                '}';
    }
}
