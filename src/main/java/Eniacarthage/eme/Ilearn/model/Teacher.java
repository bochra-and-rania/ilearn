package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.Set;

@Entity

@Table(name = "teacher")
public class Teacher extends User {
    @Column(name = "schoolSubject")
    private String schoolSubject;
    @Column(name = "speciality")
    private String speciality;
    @Column(name = "description")
    private String description;


    @OneToMany(mappedBy = "teacher")
    private Set<Training> Trainings;

    @ManyToMany(mappedBy = "teachers", fetch = FetchType.EAGER)
    private Set<Comments> comments;


    public Teacher(int id, String firstName, String lastName, String password, String eMail, String birth, String country, String city, String occupation, String sex, String schoolSubject, String speciality, String description, Set<Training> trainings, Set<Comments> comments) {
        super(id, firstName, lastName, password, eMail, birth, country, city, occupation, sex);

        this.schoolSubject = schoolSubject;
        this.speciality = speciality;
        this.description = description;
        this.Trainings = trainings;
        this.comments = comments;
    }

    public Teacher() {
        super();
    }

    public Set<Comments> getComments() {
        return comments;
    }

    public void setComments(Set<Comments> comments) {
        this.comments = comments;
    }

    public String getSchoolSubject() {
        return schoolSubject;
    }

    public void setSchoolSubject(String schoolSubject) {
        this.schoolSubject = schoolSubject;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Training> getTrainings() {
        return Trainings;
    }

    public void setTrainings(Set<Training> Trainings) {
        this.Trainings = Trainings;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                ", schoolSubject='" + schoolSubject + '\'' +
                ", speciality='" + speciality + '\'' +
                ", description='" + description + '\'' +
                ", Trainings=" + Trainings +
                ", comments=" + comments +
                '}';
    }
}
