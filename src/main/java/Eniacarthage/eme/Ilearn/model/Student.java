package Eniacarthage.eme.Ilearn.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "Student")

public class Student extends User {


    @Column(name = "nameSchool")
    private String nameSchool;

    @ManyToMany(mappedBy = "students")
    private Collection<Test> tests = new ArrayList<>();

    @ManyToMany(mappedBy = "students1")
    private Collection<Comments> comments = new ArrayList<>();

    @OneToMany(mappedBy = "student")
    private Collection<Inscription> inscriptions;


    public Student(int id, String firstName, String lastName, String password, String eMail, String birth, String country, String city, String occupation, String sex, int id1, String nameSchool, Collection<Test> tests, Collection<Comments> comments, Collection<Inscription> inscriptions) {
        super(id, firstName, lastName, password, eMail, birth, country, city, occupation, sex);


        this.nameSchool = nameSchool;
        this.tests = tests;
        this.comments = comments;
        this.inscriptions = inscriptions;
    }

    public Student() {
    }

    public String getNameSchool() {
        return nameSchool;
    }

    public void setNameSchool(String nameSchool) {
        this.nameSchool = nameSchool;
    }

    public Collection<Test> getTests() {
        return tests;
    }

    public void setTests(Collection<Test> tests) {
        this.tests = tests;
    }

    public Collection<Comments> getComments() {
        return comments;
    }

    public void setComments(Collection<Comments> comments) {
        this.comments = comments;
    }

    public Collection<Inscription> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(Collection<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    @Override
    public String toString() {
        return "Student{" +


                ", nameSchool='" + nameSchool + '\'' +
                ", tests=" + tests +
                ", comments=" + comments +
                ", inscriptions=" + inscriptions +
                '}';
    }
}



