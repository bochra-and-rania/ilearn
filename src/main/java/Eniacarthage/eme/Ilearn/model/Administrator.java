package Eniacarthage.eme.Ilearn.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "adminstrator")
public class Administrator extends User {
    public Administrator(int id, String firstName, String lastName, String password, String eMail, String birth, String country, String city, String occupation, String sex) {
        super(id, firstName, lastName, password, eMail, birth, country, city, occupation, sex);
    }

    public Administrator() {
    }

    @Override
    public String toString() {
        return "Adminstrator{}";
    }
}
